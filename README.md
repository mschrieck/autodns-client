
!!!!!!!!!!!!!!!!!!!!!!!!!
This is under Development
!!!!!!!!!!!!!!!!!!!!!!!!!


Requires a Valid AutoDNS Account

Usage:

	$com = new AutoDNS\DefaultCommunicator();
	$com->setUrl($config['url']);
	$com->setUserAgent("AutoDNS Client Example");

	// Config Tasks 
	$auth = new AutoDNS\Auth($com);
	$auth->setUser($config['user']);
	$auth->setContext($config['context']);
	$auth->setPassword($config['password']);

    // prepare a DomainContact and create it
    $ownerc=new DomainContact($auth);
    $ownerc->setLname("lname");
    $response=$ownerc->doCreate();
    if (!$response->isError()) {
        $id=$ownerc->getId();
    }else{
        print $response->prettyPrintMessages();
    }
    

