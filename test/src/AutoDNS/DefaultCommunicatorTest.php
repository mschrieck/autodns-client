<?php

namespace AutoDNS;

require_once 'AutoDNS/DefaultCommunicator.php';

/**
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class DefaultCommunicatorTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var DefaultCommunicator
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new DefaultCommunicator;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers AutoDNS\DefaultCommunicator::getUrl
 	 * @covers AutoDNS\DefaultCommunicator::setUrl
	 */
	public function testSetGetUrl() {
		$url="http://127.1.2.3:11111/dummy";
		$this->object->setUrl($url);
		$this->assertEquals($url,$this->object->getUrl());
	}

	/**
	 * @covers AutoDNS\DefaultCommunicator::getUserAgent
	 * @covers AutoDNS\DefaultCommunicator::setUserAgent
	 */
	public function testSetGetUserAgent() {
		$ua="Test Client";
		$this->object->setUserAgent($ua);
		$this->assertEquals($ua,$this->object->getUserAgent());
	}

}
