<?php

namespace AutoDNS;

require_once("AutoDNS/TestCommunicator.php");

/**
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class TestCommunicatorTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var TestCommunicator
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new TestCommunicator;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers AutoDNS\TestCommunicator::send
	 */
	public function testSend() {

		$this->object->setResponse("TEST RESPONSE");

		$response = $this->object->send("TEST REQUEST");

		$request = $this->object->getRequest();

		$this->assertEquals($request, "TEST REQUEST");

		$this->assertEquals($response, "TEST RESPONSE");
	}

}
