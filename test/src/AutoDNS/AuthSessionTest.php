<?php

namespace AutoDNS;

require_once 'AutoDNS/AuthSession.php';
require_once 'AutoDNS/Auth.php';
require_once 'AutoDNS/TestCommunicator.php';

/**
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class AuthSessionTest extends \PHPUnit_Framework_TestCase {

	private $testHash = array(
		"user" => array(
			"user" => 'marco',
			"context" => '4'
		),
		"ip" => "1.2.3.4",
		"user_agent" => "firefox",
		"timeout" => "10"

	);

	/**
	 * @var AuthSession
	 */
	protected $object;
	protected $mockCommunicator;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->mockCommunicator = new TestCommunicator();
		$mockAuth = new Auth($this->mockCommunicator);
		$this->object = new AuthSession($mockAuth);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers AutoDNS\AuthSession::fromArray
	 */
	public function testHashConversion() {
		$this->object->fromArray($this->testHash);
		$hash = $this->object->getData();

				print "\n=========\n";
		print_r($this->testHash);

		print "\n=========\n";

		print_r($hash);
		print "\n=========\n";
		
		$this->assertEquals($this->testHash, $hash);
	}

	/**
	 * @covers AutoDNS\AuthSession::doCreate
	 */
	public function testDoCreate() {

		$requestXML = file_get_contents('resource/auth-session-create-request.xml');
		$responseXML = file_get_contents('resource/auth-session-create-response.xml');

		$hash = $this->testHash;
		unset($hash['id']);
		$this->object->fromArray($hash);

		$this->mockCommunicator->setResponse($responseXML);
		$response = $this->object->doCreate();
		$request = $this->mockCommunicator->getRequest();

		$this->assertEquals( "xxxx", $this->object->getHash());
		$this->assertEquals($response->isSuccess(), true);
		$this->assertXmlStringEqualsXmlString($requestXML, $request);
	}

	

	/**
	 * @covers AutoDNS\AuthSession::doDelete
	 */
	public function testDoDelete() {
		$requestXML = file_get_contents('resource/auth-session-delete-request.xml');
		$responseXML = file_get_contents('resource/auth-session-delete-response.xml');

		$hash = array('hash' => "xxxx");
		$this->object->fromArray($hash);

		$this->mockCommunicator->setResponse($responseXML);
		$response = $this->object->doDelete();
		$request = $this->mockCommunicator->getRequest();

		$this->assertEquals($response->isSuccess(), true);
		$this->assertXmlStringEqualsXmlString($request, $requestXML);
	}

}
