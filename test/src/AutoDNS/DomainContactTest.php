<?php

namespace AutoDNS;

require_once 'AutoDNS/DomainContact.php';
require_once 'AutoDNS/Auth.php';
require_once 'AutoDNS/TestCommunicator.php';

/**
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class DomainContactTest extends \PHPUnit_Framework_TestCase {

	private $testHash = array(
		"extension"=>array(),
		"id" => "21606546",
		"lname" => "lname",
		"fname" => "fname",
		"title" => "title",
		"type" => "ORG",
		"organization" => "organization",
		"address" => "maximilian strasse 6",
		"city" => "regensburg",
		"pcode" => "93047",
		"state" => "bayern",
		"country" => "DE",
		"phone" => "+49-941-123123",
		"fax" => "+49-941-123123",
		"email" => "info@example.com",
		"sip" => "",
		"protection" => "B",
		"extension" => array(
			"vatnumber" => 'DE123241243'
		)
		
	);

	/**
	 * @var DomainContact
	 */
	protected $object;
	protected $mockCommunicator;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->mockCommunicator = new TestCommunicator();
		$mockAuth = new Auth($this->mockCommunicator);
		$this->object = new DomainContact($mockAuth);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers AutoDNS\DomainContact::fromArray
	 */
	public function testHashConversion() {
		$this->object->fromArray($this->testHash);
		$hash = $this->object->getData();

				print "\n=========\n";
		print_r($this->testHash);

		print "\n=========\n";

		print_r($hash);
		print "\n=========\n";
		
		$this->assertEquals($this->testHash, $hash);
	}

	/**
	 * @covers AutoDNS\DomainContact::doCreate
	 */
	public function testDoCreate() {

		$requestXML = file_get_contents('resource/domain-contact-create-request.xml');
		$responseXML = file_get_contents('resource/domain-contact-create-response.xml');

		$hash = $this->testHash;
		unset($hash['id']);
		$this->object->fromArray($hash);

		$this->mockCommunicator->setResponse($responseXML);
		$response = $this->object->doCreate();
		$request = $this->mockCommunicator->getRequest();

		$this->assertEquals($this->object->getId(), 21606546);
		$this->assertEquals($response->isNotify(), true);
		$this->assertXmlStringEqualsXmlString($request, $requestXML);
	}

	/**
	 * @covers AutoDNS\DomainContact::doInfo
	 */
	public function testDoInfo() {
		$requestXML = file_get_contents('resource/domain-contact-info-request.xml');
		$responseXML = file_get_contents('resource/domain-contact-info-response.xml');

		$hash = array('id' => 21606546);

		
		$this->object->fromArray($hash);

		$this->mockCommunicator->setResponse($responseXML);
		$response = $this->object->doInfo();
		$request = $this->mockCommunicator->getRequest();

		$this->assertEquals($response->isSuccess(), true);
		$this->assertXmlStringEqualsXmlString( $requestXML, $request);
		
		$hash=$this->object->getData();
		// Fix because silly XML Compare
		$hash['sip']='';
		$this->assertEquals( $this->testHash, $hash);
	}

	/**
	 * @covers AutoDNS\DomainContact::doUpdate
	 */
	public function testDoUpdate() {

		$requestXML = file_get_contents('resource/domain-contact-update-request.xml');
		$responseXML = file_get_contents('resource/domain-contact-update-response.xml');

		$hash = $this->testHash;
		$this->object->fromArray($hash);

		$this->mockCommunicator->setResponse($responseXML);
		$response = $this->object->doUpdate();
		$request = $this->mockCommunicator->getRequest();

		$this->assertEquals($response->isSuccess(), true);
		$this->assertXmlStringEqualsXmlString($request, $requestXML);
	}

	/**
	 * @covers AutoDNS\DomainContact::doDelete
	 */
	public function testDoDelete() {
		$requestXML = file_get_contents('resource/domain-contact-delete-request.xml');
		$responseXML = file_get_contents('resource/domain-contact-delete-response.xml');

		$hash = array('id' => 21606546);
		$this->object->fromArray($hash);

		$this->mockCommunicator->setResponse($responseXML);
		$response = $this->object->doDelete();
		$request = $this->mockCommunicator->getRequest();

		$this->assertEquals($response->isSuccess(), true);
		$this->assertXmlStringEqualsXmlString($request, $requestXML);
	}

}
