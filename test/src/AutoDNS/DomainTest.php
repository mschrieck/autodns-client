<?php

namespace AutoDNS;

require_once 'AutoDNS/Domain.php';
require_once 'AutoDNS/Auth.php';
require_once 'AutoDNS/TestCommunicator.php';

/**
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class DomainTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var Domain
	 */
	protected $object;
	protected $mockCommunicator;

	private $testHash = array(
		"name" => "example.com",
		"payable" => "2015-12-04 12:08:17",
		"ownerc" => "21397561",
		"adminc" => "21397562",
		"techc" => "21397563",
		"zonec" => "21397564",
		"registry_status" => "HOLD",
		"nserver" => array(array("name"=>"ns1.example.net"),array("name"=>"ns2.example.net")),
		"authinfo"=>'fQB/jZ5HgWGB857W',
		"registrar_status" => "HOLD",
		"confirm_order" => 0,
		"period"=> 1,
		"use_privacy"=> 1,
		"use_autodnssec"=>0
		

	);
	/*
				<domain>
				<name>example.com</name>

				
				<period>1</period>
				<status>SUCCESS</status>
				<use_trustee>false</use_trustee>
				<use_privacy>false</use_privacy>
				<authinfo>fQB/jZ5HgWGB857W</authinfo>
				<domainsafe>false</domainsafe>
				<cancelation>PREACK_EXPIRE</cancelation>
				<autorenew>true</autorenew>
				<extension/>
				<registrar_status>HOLD</registrar_status>
				<registrar_status_reason>ocval</registrar_status_reason>
				<owner>
					<user>user</user>
					<context>4</context>
				</owner>
				<updater>
					<user>support</user>
					<context>1</context>
				</updater>
				<created>2013-12-04 12:08:17</created>
				<updated>2015-10-21 12:38:40</updated>
			</domain>
	
	 */
	
	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->mockCommunicator = new TestCommunicator();
		$mockAuth = new Auth($this->mockCommunicator);
		$this->object = new Domain($mockAuth);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers AutoDNS\Domain::getUsePrivacy
	 * @todo   Implement testGetUsePrivacy().
	 */
	public function testGetUsePrivacy() {
		
	}

	/**
	 * @covers AutoDNS\Domain::getUseAutoDNSSEC
	 * @todo   Implement testGetUseAutoDNSSEC().
	 */
	public function testGetUseAutoDNSSEC() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setUsePrivacy
	 * @todo   Implement testSetUsePrivacy().
	 */
	public function testSetUsePrivacy() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setUseAutoDNSSEC
	 * @todo   Implement testSetUseAutoDNSSEC().
	 */
	public function testSetUseAutoDNSSEC() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getDomainsafe
	 * @todo   Implement testGetDomainsafe().
	 */
	public function testGetDomainsafe() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getNsGroup
	 * @todo   Implement testGetNsGroup().
	 */
	public function testGetNsGroup() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getRegistrarStatus
	 * @todo   Implement testGetRegistrarStatus().
	 */
	public function testGetRegistrarStatus() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getRegistryStatus
	 * @todo   Implement testGetRegistryStatus().
	 */
	public function testGetRegistryStatus() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setDomainsafe
	 * @todo   Implement testSetDomainsafe().
	 */
	public function testSetDomainsafe() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setNsGroup
	 * @todo   Implement testSetNsGroup().
	 */
	public function testSetNsGroup() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setRegistrarStatus
	 * @todo   Implement testSetRegistrarStatus().
	 */
	public function testSetRegistrarStatus() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setRegistryStatus
	 * @todo   Implement testSetRegistryStatus().
	 */
	public function testSetRegistryStatus() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getName
	 * @todo   Implement testGetName().
	 */
	public function testGetName() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getOwnerc
	 * @todo   Implement testGetOwnerc().
	 */
	public function testGetOwnerc() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getAdminc
	 * @todo   Implement testGetAdminc().
	 */
	public function testGetAdminc() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getTechc
	 * @todo   Implement testGetTechc().
	 */
	public function testGetTechc() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getZonec
	 * @todo   Implement testGetZonec().
	 */
	public function testGetZonec() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getNserver
	 * @todo   Implement testGetNserver().
	 */
	public function testGetNserver() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getAuthinfo
	 * @todo   Implement testGetAuthinfo().
	 */
	public function testGetAuthinfo() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getPeriod
	 * @todo   Implement testGetPeriod().
	 */
	public function testGetPeriod() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getExpireDate
	 * @todo   Implement testGetExpireDate().
	 */
	public function testGetExpireDate() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getCreateDate
	 * @todo   Implement testGetCreateDate().
	 */
	public function testGetCreateDate() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getConfirmOrder
	 * @todo   Implement testGetConfirmOrder().
	 */
	public function testGetConfirmOrder() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::getRemoveCancelation
	 * @todo   Implement testGetRemoveCancelation().
	 */
	public function testGetRemoveCancelation() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setName
	 * @todo   Implement testSetName().
	 */
	public function testSetName() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setOwnerc
	 * @todo   Implement testSetOwnerc().
	 */
	public function testSetOwnerc() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setAdminc
	 * @todo   Implement testSetAdminc().
	 */
	public function testSetAdminc() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setTechc
	 * @todo   Implement testSetTechc().
	 */
	public function testSetTechc() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setZonec
	 * @todo   Implement testSetZonec().
	 */
	public function testSetZonec() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setNserver
	 * @todo   Implement testSetNserver().
	 */
	public function testSetNserver() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setAuthinfo
	 * @todo   Implement testSetAuthinfo().
	 */
	public function testSetAuthinfo() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setPeriod
	 * @todo   Implement testSetPeriod().
	 */
	public function testSetPeriod() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setExpireDate
	 * @todo   Implement testSetExpireDate().
	 */
	public function testSetExpireDate() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setCreateDate
	 * @todo   Implement testSetCreateDate().
	 */
	public function testSetCreateDate() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setConfirmOrder
	 * @todo   Implement testSetConfirmOrder().
	 */
	public function testSetConfirmOrder() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::setRemoveCancelation
	 * @todo   Implement testSetRemoveCancelation().
	 */
	public function testSetRemoveCancelation() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::doCreate
	 * @todo   Implement testDoCreate().
	 */
	public function testDoCreate() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::doUpdate
	 * @todo   Implement testDoUpdate().
	 */
	public function testDoUpdate() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::doUpdateStatus
	 * @todo   Implement testDoUpdateStatus().
	 */
	public function testDoUpdateStatus() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::doInfo
	 * @todo   Implement testDoInfo().
	 */
	public function testDoInfo() {
		$requestXML = file_get_contents('resource/domain-info-request.xml');
		$responseXML = file_get_contents('resource/domain-info-response.xml');

		$hash = array('name' => 'example.com');

		
		$this->object->fromArray($hash);

		$this->mockCommunicator->setResponse($responseXML);
		$response = $this->object->doInfo();
		$request = $this->mockCommunicator->getRequest();
		$this->assertEquals($response->isSuccess(), true);
		$this->assertXmlStringEqualsXmlString( $requestXML, $request);
		

		$this->assertEquals( $this->testHash, $this->object->getData());
	}

	/**
	 * @covers AutoDNS\Domain::doTransfer
	 * @todo   Implement testDoTransfer().
	 */
	public function testDoTransfer() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::doRenew
	 * @todo   Implement testDoRenew().
	 */
	public function testDoRenew() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Domain::doRestore
	 * @todo   Implement testDoRestore().
	 */
	public function testDoRestore() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

}
