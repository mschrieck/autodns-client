<?php

namespace AutoDNS;

require_once 'AutoDNS/Response.php';

/**
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class ResponseTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var Response
	 */
	protected $object;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->object = new Response;
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers Response::parse success Message
	 */
	public function testParseResponseSuccess() {
		$xml = file_get_contents("resource/response-success-example.xml");

		$this->object->parse($xml);
		$this->assertEquals($this->object->isSuccess(), true, "isSuccess");
		$this->assertEquals($this->object->getStatus(), 'success');
		$this->assertEquals($this->object->getStatusText(), 'status message');
		$this->assertEquals($this->object->getStatusCode(), 'S0000');
		$this->assertEquals($this->object->getStatusObjectType(), 'object-type');
		$this->assertEquals($this->object->getStatusObjectValue(), 'object-value');
		$this->assertEquals($this->object->getDataAsArray(), array('dummy' => 'dummydata'));
		$this->assertEquals($this->object->getData()->dummy, 'dummydata');
		$this->assertEquals($this->object->getCTID(), 'ctid');
		$this->assertEquals($this->object->getSTID(), 'stid');
	}

	/**
	 * @covers Response::isTempError
	 * @todo   Implement testIsTempError().
	 */
	public function testIsTempError() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers Response::isError
	 * @todo   Implement testIsError().
	 */
	public function testIsError() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers Response::isNotify
	 * @todo   Implement testIsNotify().
	 */
	public function testIsNotify() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

}
