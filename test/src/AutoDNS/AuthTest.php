<?php

namespace AutoDNS;

require_once 'AutoDNS/TestCommunicator.php';
require_once 'AutoDNS/Auth.php';

class AuthTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var Auth
	 */
	protected $object;
	protected $mockCommunicator;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {

		$this->mockCommunicator = new TestCommunicator();
		$this->object = new Auth($this->mockCommunicator);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers AutoDNS\Auth::setCommunicator
	 * @todo   Implement testSetCommunicator().
	 */
	public function testSetCommunicator() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::getCommunicator
	 * @todo   Implement testGetCommunicator().
	 */
	public function testGetCommunicator() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::getUser
	 * @todo   Implement testGetUser().
	 */
	public function testGetUser() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::getContext
	 * @todo   Implement testGetContext().
	 */
	public function testGetContext() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::getPassword
	 * @todo   Implement testGetPassword().
	 */
	public function testGetPassword() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::getOwnerUser
	 * @todo   Implement testGetOwnerUser().
	 */
	public function testGetOwnerUser() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::getOwnerContext
	 * @todo   Implement testGetOwnerContext().
	 */
	public function testGetOwnerContext() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::getLanguage
	 * @todo   Implement testGetLanguage().
	 */
	public function testGetLanguage() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::setUser
	 * @todo   Implement testSetUser().
	 */
	public function testSetUser() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::setContext
	 * @todo   Implement testSetContext().
	 */
	public function testSetContext() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::setPassword
	 * @todo   Implement testSetPassword().
	 */
	public function testSetPassword() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::setOwnerUser
	 * @todo   Implement testSetOwnerUser().
	 */
	public function testSetOwnerUser() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::setOwnerContext
	 * @todo   Implement testSetOwnerContext().
	 */
	public function testSetOwnerContext() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

	/**
	 * @covers AutoDNS\Auth::setLanguage
	 * @todo   Implement testSetLanguage().
	 */
	public function testSetLanguage() {
		// Remove the following lines when you implement this test.
		$this->markTestIncomplete(
				'This test has not been implemented yet.'
		);
	}

}
