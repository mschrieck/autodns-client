<?php

namespace AutoDNS;

require_once 'AutoDNS/TestCommunicator.php';
require_once 'AutoDNS/AbstractTask.php';
require_once 'AutoDNS/Auth.php';

class TaskBasicAbstractTest extends AbstractTask {
	
	public function fromArray(array $array) {
		
	}

}

/**
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class TaskTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var AbstractTask
	 */
	protected $object;
	protected $mockCommunicator;

	/**
	 * Sets up the fixture, for example, opens a network connection.
	 * This method is called before a test is executed.
	 */
	protected function setUp() {
		$this->mockCommunicator = new TestCommunicator();
		$mockAuth = new Auth($this->mockCommunicator);
		$this->object = new TaskBasicAbstractTest($mockAuth);
	}

	/**
	 * Tears down the fixture, for example, closes a network connection.
	 * This method is called after a test is executed.
	 */
	protected function tearDown() {
		
	}

	/**
	 * @covers AutoDNS\Task::send
	 */
	public function testSend() {
		$responseXML = file_get_contents("resource/response-success-example.xml");
		$requestXML = file_get_contents("resource/full-request-dummy.xml");

		$this->object->getAuth()->setUser("user");
		$this->object->getAuth()->setPassword("password");
		$this->object->getAuth()->setContext("1");
		$this->object->getAuth()->setOwnerUser("owner.user");
		$this->object->getAuth()->setOwnerContext("2");
		$this->object->getAuth()->setLanguage('de');
		$this->object->setCtid('ctid');
		$this->mockCommunicator->setResponse($responseXML);

		$test=array(
			'code'=>'0000',
			'dummy'=>''
		);
		$response = $this->object->send($test);

		$request = $this->mockCommunicator->getRequest();
		print_r($this->object);


		$this->assertXmlStringEqualsXmlString($request, $requestXML);
		$this->assertEquals($response->isSuccess(), true);
	}

}
