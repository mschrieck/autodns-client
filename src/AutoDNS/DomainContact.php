<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

require_once 'AbstractTask.php';

/**
 * Description of DomainContact
 * 
 * @todo Not All Keys are supported
 *  - Not All Extensions are supported
 *  - Alias is not Supported
 *  - DomainSafe is not Supported
 *  - NIC REFs are not Supported
 *
 * @author Marco Schrieck <mschrieck@gmail.com>
 * 
 */
class DomainContact extends AbstractTask {

	const TYPE_ORG = 'ORG';
	const TYPE_PERSON = 'PERSON';
	const TYPE_ROLE = 'ROLE';

	protected $data = array('extension' => array());

	public function __construct(Auth $auth = null) {
		parent::__construct($auth);
	}

	protected function setExtensionDataKey($key, $value) {
		$this->data['extension'][$key] = $value;
	}

	protected function getExtensionDataKey($key) {
		return $this->data['extension'][$key];
	}

	public function getId() {
		return $this->getDataKey('id');
	}

	public function setId($id) {
		$this->setDataKey('id', (string) $id);
	}

	public function getLName() {
		return $this->getDataKey('lname');
	}

	public function getFName() {
		return $this->getDataKey('fname');
	}

	public function getType() {
		return $this->getDataKey('type');
	}

	public function getOrganization() {
		return $this->getDataKey('organization');
	}

	public function getTitle() {
		return $this->getDataKey('title');
	}

	public function getAddress() {
		return $this->getDataKey('address');
	}

	public function getCity() {
		return $this->getDataKey('city');
	}

	public function getPCode() {
		return $this->getDataKey('pcode');
	}

	public function getState() {
		return $this->getDataKey('state');
	}

	public function getCountry() {
		return $this->getDataKey('country');
	}

	public function getPhone() {
		return $this->getDataKey('phone');
	}

	public function getFax() {
		return $this->getDataKey('fax');
	}

	public function getEmail() {
		return $this->getDataKey('email');
	}

	public function setEmail($email) {
		$this->setDataKey('email', $email);
	}

	public function getProtection() {
		return $this->getDataKey('protection');
	}

	public function getSip() {
		return $this->getDataKey('sip');
	}

	public function setLName($lname) {
		$this->setDataKey('lname', trim((string) $lname));
	}

	public function setFName($fname) {
		$this->setDataKey('fname', trim((string) $fname));
	}

	public function setType($type) {
		$this->setDataKey('type', (string) $type);
	}

	public function setOrganization($organization) {
		$this->setDataKey('organization', trim((string) $organization));
	}

	public function setTitle($title) {
		$this->setDataKey('title', trim((string) $title));
	}

	public function setAddress($address) {
		$this->setDataKey('address', trim((string) $address));
	}

	public function setCity($city) {
		$this->setDataKey('city', trim((string) $city));
	}

	public function setPCode($pCode) {
		$this->setDataKey('pcode', trim((string) $pCode));
	}

	public function setState($state) {
		$this->setDataKey('state', trim((string) $state));
	}

	public function setCountry($country) {
		$this->setDataKey('country', strtoupper(trim((string) $country)));
	}

	public function setPhone($phone) {
		$this->setDataKey('phone', trim((string) $phone));
	}

	public function setFax($fax) {
		$this->setDataKey('fax', trim((string) $fax));
	}

	public function setProtection($protection) {
		$this->setDataKey('protection', (string) $protection);
	}

	public function setSip($sip) {
		$this->setDataKey('sip', trim($sip));
	}

	public function setExtMobile($value) {
		$this->setExtensionDataKey('mobile', (string) $value);
	}

	public function setExtVatNumber($value) {
		$this->setExtensionDataKey('vatnumber', (string) $value);
	}

	public function setExtAeroEnsAuthId($value) {
		$this->setExtensionDataKey('aero_ens_auth_id', (string) $value);
	}

	public function setExtAeroEnsKey($value) {
		$this->setExtensionDataKey('aero_ens_key', (string) $value);
	}

	public function setExtBarcelonaIntendedUse($value) {
		$this->setExtensionDataKey('barcelona_intended_use', (string) $value);
	}

	public function setExtBirthCountry($value) {
		$this->setExtensionDataKey('birthcountry', (string) $value);
	}

	public function setExtBirthday($value) {
		$this->setExtensionDataKey('birthday', (string) $value);
	}

	public function setExtBirthPCode($value) {
		$this->setExtensionDataKey('birthpcode', (string) $value);
	}

	public function setExtBirthplace($value) {
		$this->setExtensionDataKey('birthplace', (string) $value);
	}

	public function setExtLanguage($value) {
		$this->setExtensionDataKey('language', (string) $value);
	}

	public function setExtCatIntendedUse($value) {
		$this->setExtensionDataKey('cat_intended_use', (string) $value);
	}

	public function setExtCompanyNumber($value) {
		$this->setExtensionDataKey('companynumber', (string) $value);
	}

	public function setExtGender($value) {
		$this->setExtensionDataKey('gender', (string) $value);
	}

	public function setExtIDNumber($value) {
		$this->setExtensionDataKey('idnumber', (string) $value);
	}

	public function setExtIDAuthority($value) {
		$this->setExtensionDataKey('id_authority', (string) $value);
	}

	public function setExtIDDateOfIssue($value) {
		$this->setExtensionDataKey('id_date_of_issue', (string) $value);
	}

	public function setExtIDValidTill($value) {
		$this->setExtensionDataKey('id_valid_till', (string) $value);
	}

	public function setExtItEntityType($value) {
		$this->setExtensionDataKey('it_entity_type', (string) $value);
	}

	public function setExtJobsAdminType($value) {
		$this->setExtensionDataKey('jobs_admin_type', (string) $value);
	}

	public function setExtJobsContactTitle($value) {
		$this->setExtensionDataKey('jobs_contact_title', (string) $value);
	}

	public function setExtJobsHRMember($value) {
		$this->setExtensionDataKey('jobs_hr_member', (string) $value);
	}

	public function setExtJobsIndustryClass($value) {
		$this->setExtensionDataKey('jobs_industry_class', (string) $value);
	}

	public function setExtJobsWebsite($value) {
		$this->setExtensionDataKey('jobs_website', (string) $value);
	}

	public function setExtTravelUIN($value) {
		$this->setExtensionDataKey('travel_uin', (string) $value);
	}

	public function setExtTrademarkName($value) {
		$this->setExtensionDataKey('trademark_name', (string) $value);
	}

	public function setExtTrademarkNumber($value) {
		$this->setExtensionDataKey('trademark_number', (string) $value);
	}

	public function setExtTrademarkCountry($value) {
		$this->setExtensionDataKey('trademark_country', (string) $value);
	}

	public function setExtTrademarkRegDate($value) {
		$this->setExtensionDataKey('trademark_regdate', (string) $value);
	}

	public function setExtTrademarkAppDate($value) {
		$this->setExtensionDataKey('trademark_appdate', (string) $value);
	}

	public function setExtTrademarkOffice($value) {
		$this->setExtensionDataKey('trademark_office', (string) $value);
	}

	public function setExtXxxMembershipId($value) {
		$this->setExtensionDataKey('xxx_membership_id', (string) $value);
	}

	public function setExtXxxMembershipPassword($value) {
		$this->setExtensionDataKey('xxx_membership_password', (string) $value);
	}

	public function setExtRoPersonType($value) {
		$this->setExtensionDataKey('ro_person_type', (string) $value);
	}

	public function setExtSwissEnterpriseId($value) {
		$this->setExtensionDataKey('swiss_enterprise_id', (string) $value);
	}

	public function setExtSwissIntendedUse($value) {
		$this->setExtensionDataKey('swiss_intended_use', (string) $value);
	}

	public function getExtMobile() {
		return $this->getExtensionDataKey('mobile');
	}

	public function getExtVatNumber() {
		return $this->getExtensionDataKey('vatnumber');
	}

	public function getExtAeroEnsAuthId() {
		return $this->getExtensionDataKey('aero_ens_auth_id');
	}

	public function getExtAeroEnsKey() {
		return $this->getExtensionDataKey('aero_ens_key');
	}

	public function getExtBarcelonaIntendedUse() {
		return $this->getExtensionDataKey('barcelona_intended_use');
	}

	public function getExtBirthCountry() {
		return $this->getExtensionDataKey('birthcountry');
	}

	public function getExtBirthday() {
		return $this->getExtensionDataKey('birthday');
	}

	public function getExtBirthPCode() {
		return $this->getExtensionDataKey('birthpcode');
	}

	public function getExtBirthplace() {
		return $this->getExtensionDataKey('birthplace');
	}

	public function getExtLanguage() {
		return $this->getExtensionDataKey('language');
	}

	public function getExtCatIntendedUse() {
		return $this->getExtensionDataKey('cat_intended_use');
	}

	public function getExtCompanyNumber() {
		return $this->getExtensionDataKey('companynumber');
	}

	public function getExtGender() {
		return $this->getExtensionDataKey('gender');
	}

	public function getExtIDNumber() {
		return $this->getExtensionDataKey('idnumber');
	}

	public function getExtIDAuthority() {
		return $this->getExtensionDataKey('id_authority');
	}

	public function getExtIDDateOfIssue() {
		return $this->getExtensionDataKey('id_date_of_issue');
	}

	public function getExtIDValidTill() {
		return $this->getExtensionDataKey('id_valid_till');
	}

	public function getExtItEntityType() {
		return $this->getExtensionDataKey('it_entity_type');
	}

	public function getExtJobsAdminType() {
		return $this->getExtensionDataKey('jobs_admin_type');
	}

	public function getExtJobsContactTitle() {
		return $this->getExtensionDataKey('jobs_contact_title');
	}

	public function getExtJobsHRMember() {
		return $this->getExtensionDataKey('jobs_hr_member');
	}

	public function getExtJobsIndustryClass() {
		return $this->getExtensionDataKey('jobs_industry_class');
	}

	public function getExtJobsWebsite() {
		return $this->getExtensionDataKey('jobs_website');
	}

	public function getExtTravelUIN() {
		return $this->getExtensionDataKey('travel_uin');
	}

	public function getExtTrademarkName() {
		return $this->getExtensionDataKey('trademark_name');
	}

	public function getExtTrademarkNumber() {
		return $this->getExtensionDataKey('trademark_number');
	}

	public function getExtTrademarkCountry() {
		return $this->getExtensionDataKey('trademark_country');
	}

	public function getExtTrademarkRegDate() {
		return $this->getExtensionDataKey('trademark_regdate');
	}

	public function getExtTrademarkAppDate() {
		return $this->getExtensionDataKey('trademark_appdate');
	}

	public function getExtTrademarkOffice() {
		return $this->getExtensionDataKey('trademark_office');
	}

	public function getExtXxxMembershipId() {
		return $this->getExtensionDataKey('xxx_membership_id');
	}

	public function getExtXxxMembershipPassword() {
		return $this->getExtensionDataKey('xxx_membership_password');
	}

	public function getExtRoPersonType() {
		return $this->getExtensionDataKey('ro_person_type');
	}

	public function getExtSwissEnterpriseId() {
		return $this->getExtensionDataKey('swiss_enterprise_id');
	}

	public function getExtSwissIntendedUse() {
		return $this->getExtensionDataKey('swiss_intended_use');
	}

	/*	 * * */

	public function fromArray(Array $hash) {
		if ($this->existsAndNotEmpty($hash, 'id')) {
			$this->setId($hash["id"]);
		}
		if ($this->existsAndNotEmpty($hash, 'type')) {
			$this->setType($hash["type"]);
		}
		if ($this->existsAndNotEmpty($hash, 'fname')) {
			$this->setFName($hash["fname"]);
		}
		if ($this->existsAndNotEmpty($hash, 'lname')) {
			$this->setLName($hash["lname"]);
		}
		if ($this->existsAndNotEmpty($hash, 'title')) {
			$this->setTitle($hash["title"]);
		}
		if ($this->existsAndNotEmpty($hash, 'organization')) {
			$this->setOrganization($hash["organization"]);
		}
		if ($this->existsAndNotEmpty($hash, 'address')) {
			$this->setAddress($hash["address"]);
		}
		if ($this->existsAndNotEmpty($hash, 'pcode')) {
			$this->setPCode($hash["pcode"]);
		}
		if ($this->existsAndNotEmpty($hash, 'city')) {
			$this->setCity($hash["city"]);
		}
		if ($this->existsAndNotEmpty($hash, 'state')) {
			$this->setState($hash["state"]);
		}
		if ($this->existsAndNotEmpty($hash, 'country')) {
			$this->setCountry($hash["country"]);
		}
		if ($this->existsAndNotEmpty($hash, 'phone')) {
			$this->setPhone($hash["phone"]);
		}
		if ($this->existsAndNotEmpty($hash, 'fax')) {
			$this->setFax($hash["fax"]);
		}
		if ($this->existsAndNotEmpty($hash, 'email')) {
			$this->setEmail($hash["email"]);
		}
		if ($this->existsAndNotEmpty($hash, 'sip')) {
			$this->setSip($hash["sip"]);
		}
		if ($this->existsAndNotEmpty($hash, 'protection')) {
			$this->setProtection($hash["protection"]);
		}
		if ($this->existsAndNotEmpty($hash, 'extension')) {
			$this->setDataKey("extension", $hash["extension"]);
		}
	}

	private function transform($code) {
		if ($code === '0301' or $code === '0302') {
			// Full data
			$data = $this->getData();
		} else {
			// Only Primary Key
			$data = array(
				'id' => $this->getId()
			);
		}
		return $hash = array(
			'code' => $code,
			'handle' => $data
		);
	}

	public function doCreate() {
		$request = $this->transform("0301");
		$response = $this->send($request);
		$this->setId($response->getStatusObjectValue());
		return $response;
	}

	public function doUpdate() {
		$request = $this->transform("0302");
		$response = $this->send($request);
		return $response;
	}

	public function doInfo() {
		$request = $this->transform("0304");
		$response = $this->send($request);
		$hash = $response->getDataAsArray();
		$this->fromArray($hash['handle']);
		return $response;
	}

	public function doDelete() {
		$request = $this->transform("0303");
		$response = $this->send($request);
		return $response;
	}

	public function parseListObject(Auth $auth, $listEntry) {
		$obj = new DomainContact($auth);
		$obj->fromArray($listEntry);
		return $obj;
	}

}
