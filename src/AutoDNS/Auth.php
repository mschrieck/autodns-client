<?php


/*
Copyright (c) 2015 Marco Schrieck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/


namespace AutoDNS;

require_once 'ICommunicator.php';
require_once 'Response.php';

/**
 * Description of Communication
 *
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class Auth {

	private $communicator;
	private $user;
	private $context;
	private $password;
	private $ownerUser;
	private $ownerContext;
	private $language;

	public final function __construct(ICommunicator $communicator) {
		$this->communicator = $communicator;
	}

	public function setCommunicator(ICommunicator $communicator) {
		$this->communicator = $communicator;
	}

	public function getCommunicator() {
		return $this->communicator;
	}

	public final function getUser() {
		return $this->user;
	}

	public final function getContext() {
		return $this->context;
	}

	public final function getPassword() {
		return $this->password;
	}

	public final function getOwnerUser() {
		return $this->ownerUser;
	}

	public final function getOwnerContext() {
		return $this->ownerContext;
	}

	public final function getLanguage() {
		return $this->language;
	}

	public final function setUser($user) {
		$this->user = $user;
	}

	public final function setContext($context) {
		$this->context = $context;
	}

	public final function setPassword($password) {
		$this->password = $password;
	}

	public final function setOwnerUser($ownerUser) {
		$this->ownerUser = $ownerUser;
	}

	public final function setOwnerContext($ownerContext) {
		$this->ownerContext = $ownerContext;
	}

	public final function setLanguage($language) {
		$this->language = $language;
	}

}


