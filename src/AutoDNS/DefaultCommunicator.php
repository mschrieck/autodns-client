<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

require_once 'ICommunicator.php';

/**
 * Description of DefaultCommunicator
 *
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class DefaultCommunicator implements ICommunicator {

	private $url = '';
	private $userAgent = "AutoDNS Client PHP";

	public function getUrl() {
		return $this->url;
	}

	public function getUserAgent() {
		return $this->userAgent;
	}

	public function setUrl($url) {
		$this->url = $url;
	}

	public function setUserAgent($userAgent) {
		$this->userAgent = $userAgent;
	}

	/*
	 * @todo
	 * SSL Options
	 */

	public function send($request) {

		$ch = \curl_init();
		\curl_setopt($ch, CURLOPT_URL, $this->url);
		\curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		\curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		\curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		\curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		\curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

		if ($xml = curl_exec($ch) ) {
			\curl_close($ch);
			return $xml;
		} else {
			\curl_close($ch);
			throw new \Exception("cant connect to AutoDNS: ".\curl_error( $ch ));
		}
	}

}
