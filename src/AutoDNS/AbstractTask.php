<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

require_once 'ICommunicator.php';
require_once 'Response.php';
require_once 'XMLTool.php';
require_once 'AbstractObject.php';


/**
 * AbstractTask defines the default Task Structure and provides a send
 *
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
abstract class AbstractTask extends AbstractObject {

	protected $auth;
	protected $ctid;

	public function __construct(Auth $auth=null) {
		$this->auth = $auth;
	}

	private final function getRequestBody($task) {

		$request = array();

		// add auth block
		if ($this->getAuth()->getUser() && $this->getAuth()->getContext() && $this->getAuth()->getPassword()) {
			$request['auth'] = array(
				'user' => $this->getAuth()->getUser(),
				'context' => $this->getAuth()->getContext(),
				'password' => $this->getAuth()->getPassword(),
			);
		}

		// add owner block
		if ($this->getAuth()->getOwnerUser() && $this->getAuth()->getOwnerContext()) {
			$request['owner'] = array(
				'user' => $this->getAuth()->getOwnerUser(),
				'context' => $this->getAuth()->getOwnerContext()
			);
		}

		// add language
		if ($lang = $this->getAuth()->getLanguage()) {
			$request['language'] = $lang;
		}


		// add ctid
		if ($ctid = $this->getCTID()) {
			$request['ctid'] = $ctid;
		}

		// add ctid
		if ($task) {
			$request['task'] = $task;
		}

		return XMLTool::createXML('request', $request);
	}

	public function send($request) {
		if (!$this->getAuth()) {
			throw new \Exception("No Auth Object set");
		}
		if (!$this->getAuth()->getCommunicator()) {
			throw new \Exception("No Communicator Object set");
		}
		$requestXML = $this->getRequestBody($request);
		// store it for debugging/Logging
		$this->request=$requestXML;
		
		$responseXml = $this->getAuth()->getCommunicator()->send($requestXML);

		$res = new Response();
		$res->setRequest($requestXML);
		$res->parse($responseXml);
		return $res;
	}

	public final function getAuth() {
		return $this->auth;
	}
	
	public final function getCTID() {
		return $this->ctid;
	}

	public final function setCTID($ctid) {
		$this->ctid = $ctid;
	}

}
