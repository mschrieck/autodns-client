<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

/**
 * Description of Communication
 *
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
abstract class AbstractObject {

	protected $data = array();

	abstract public function fromArray(Array $array);

	public final function getData() {
		return $this->data;
	}

	protected final function existsAndNotEmpty(array $array, $key) {

		if (!array_key_exists($key, $array)){
			return false;
			
		}
		$value=$array[$key];
		
		if ($value === null){
			return false;
		}
		// array exists which is not empty
		if ( is_array($value ) && count($value )=== 0 ) {
			return false;
		}
		return true;
	}

	// data key behaivor 

	protected final function getDataKey($key) {
		if ($this->existsAndNotEmpty($this->data, $key)) {
			return $this->data[$key];
		}
	}

	protected final function setDataKey($key, $value) {
		if ($value !== null) {
			$this->data[$key] = $value;
		}
	}

	protected final function unsetDataKey($key) {
		if ($this->existsAndNotEmpty($this->data, $key)) {
			unset($this->data[$key]);
		}
	}

	protected final function pushDataKey($key, $value) {

		// 
		if ($this->existsAndNotEmpty($this->data, $key) && is_array($this->data)) {
			// ok is array and set
		} else {
			// initalize 
			if ($this->existsAndNotEmpty($this->data, $key) && !is_array($this->data)) {
				// force array
				$this->data[$key] = array($this->data[$key]);
			} else {
				// initalize empty array
				$this->data[$key] = array();
			}
		}

		array_push($this->data[$key], $value);
	}

	protected final function popDataKey($key) {
		if ($this->existsAndNotEmpty($this->data, $key) && is_array($this->data)) {
			return array_pop($this->data[$key]);
		}
	}

	/*
	 * Return Object as Array
	 */

	public function toArray() {
		return $this->getData();
	}

}


