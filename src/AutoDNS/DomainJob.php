<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

class DomainJob extends AbstractObject {

	public function fromArray(array $array) {
		$keys = array('id', 'object', 'status', 'sub_status', 'created', 'updated', 'type', 'sub_type', 'action', 'execution');
		foreach ($keys as $key) {
			// prevent notices
			if (!isset($array[$key])) {
				continue;
			}
			// skip empty keys wich are parsed as array
			if (is_array($array[$key])) {
				continue;
			}
			$this->setDataKey($key, $array[$key]);
		}
	}

	public function getId() {
		return $this->getDataKey('id');
	}

	public function getObject() {
		return $this->getDataKey('object');
	}

	public function getStatus() {
		return strtolower($this->getDataKey('status'));
	}

	public function getSubStatus() {
		return strtolower($this->getDataKey('sub_status'));
	}

	public function getCreated() {
		return $this->getDataKey('created');
	}

	public function getUpdated() {
		return $this->getDataKey('updated');
	}

	public function getType() {
		return strtolower($this->getDataKey('type'));
	}

	public function getSubType() {
		return strtolower($this->getDataKey('sub_type'));
	}

	public function getAction() {
		return strtolower($this->getDataKey('action'));
	}

	public function getExecution() {
		return $this->getDataKey('execution');
	}

}
