<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

require_once 'AbstractTask.php';

/**
 * Generate a Backend Session
 *
 * @author marcoschrieck
 */
class AuthSession extends AbstractTask {

	public function getSubUser(){
		$user=$this->getDataKey('user');
		if (is_array($user)){
			return $user['user'];
		}
	}
	
	public function getSubContext(){
		$user=$this->getDataKey('user');
		if (is_array($user)){
			return $user['context'];
		}

	}
	
	public function getHash(){
		return $this->getDataKey('hash');
	}
	
	public function setHash($value){
		$this->setDataKey('hash',$value);
	}
	
	public function getIp(){
		return $this->getDataKey('ip');
	}
	
	public function getUserAgent(){
		return $this->getDataKey('user_agent');
	}
	
	public function getTimeout(){
		return $this->getDataKey('timeout');
	}
	
	public function setSubUser($value){
		$user=$this->getDataKey('user');
		if (!is_array($user)){
			$user=array();
		}
		$user['user']=$value;
		$this->setDataKey('user',$user);

	}
	
	public function setSubContext($value){
		$user=$this->getDataKey('user');
		if (!is_array($user)){
			$user=array();
		}
		$user['context']=$value;
		$this->setDataKey('user',$user);
	}
	
	public function setIp($value){
		$this->setDataKey('ip',$value);
	}
	
	public function setUserAgent($value){
		$this->setDataKey('user_agent',$value);
	}

	public function setTimeout($value){
		$this->setDataKey('timeout',$value);
	}
	
	
	private function transform($code) {
		$data = $this->data;
		return $hash = array(
			'code' => $code,
			'auth_session' => $data
		);
	}
	
	public function doCreate() {
		$request = $this->transform('1321001');
		$response=$this->send($request);
		$hash = $response->getDataAsArray();
		if ($response->isSuccess()){
			$this->fromArray($hash['auth_session']);
		}
		return $response;
	}
	
	public function doDelete() {
		$request = $this->transform('1321003');
		return $this->send($request);
	}

	public function fromArray(array $array) {
		$keys = array( 'hash', 'ip', 'user_agent', 'timeout', 'user');
		
		if(isset($array['user']['user'])){
			$this->setSubUser($array['user']['user']);
		}
		
		if(isset($array['user']['context'])){
			$this->setSubContext($array['user']['context']);
		}
		foreach ($keys as $key) {
			// prevent notices
			if (!isset($array[$key])) {
				continue;
			}
			// skip empty keys wich are parsed as array
			if (is_array($array[$key])) {
				continue;
			}
			$this->setDataKey($key, $array[$key]);
		}
		

		
	}

}
