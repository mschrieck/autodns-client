<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

require_once 'AbstractTask.php';

/**
 * 
 * DomainCancelation
 * 
 * 
 * 
 * 
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class DomainCancelation extends AbstractTask {

	/*
	 * EXECDATE_DATE for Deletions at given Date
	 */
	const EXECDATE_DATE = 'DATE';

	/*
	 *  EXECDATE_EXPIRE for Deletions at Expire
	 */
	const EXECDATE_EXPIRE = 'expire';

	/*
	 * EXECDATE_NOW for Deletions at now
	 */
	const EXECDATE_NOW = 'now';

	/*
	 * TYPE_DELETE
	 */
	const TYPE_DELETE = 'DELETE';
	
	/*
	 * TYPE_PREACK
	 */

	const TYPE_PREACK = 'PREACK';

	/*
	 * TYPE_TRANSIT
	 */
	const TYPE_TRANSIT = 'TRANSIT';
	
	/*
	 * get Domainname
	 * @return string domain
	 */
	public function getDomain() {
		return $this->getDataKey('domain');
	}
	
	/*
	 * set Domainname
	 * 
	 * @param string domain
	 */
	public function setDomain($domain) {
		$this->setDataKey('domain', $domain);
	}
	
	/*
	 * get Type of cancelation (DELETE/PREACK/TRANSIT)
	 * 
	 * @return string type
	 */
	public function getType() {
		return $this->getDataKey('type');
	}

	/*
	 * set Type of a cancelation (DELETE/PREACK/TRANSIT)
	 * 
	 * @param string type
	 */
	public function setType($type) {
		$this->setDataKey('type', $type);
	}

	/*
	 * get Execdate of a cancelation
	 * 
	 * @return string execdate
	 */
	public function getExecDate() {
		return $this->getDataKey('execdate');
	}

	/*
	 * set Execdate of a cancelation
	 * 
	 * @param string date - could be "YY-MM-DD hh:mm:ss" or "YY-MM-DD" or NOW or EXPIRE
	 */
	public function setExecDate($execDate) {
		$this->setDataKey('execdate', $execDate);
	}


	/*
	 * get Disconnect Flag for Transit
	 * 
	 * @return boolean disconnect
	 */
	public function getDisconnect() {
		return $this->getDataKey('disconnect') == 1;
	}
	
	/*
	 * set Disconnect Flag for Transit
	 * 
	 * @param boolean disconnect
	 */	
	public function setDisconnect( $disconnect) {
		$this->setDataKey('disconnect', $disconnect ? 1 : 0);
	}

	/*
	 * get created date
	 * 
	 * @return string created
	 */
	public function getCreated() {
		return $this->getDataKey('created');
	}

	/*
	 * set created date
	 * 
	 * @param string created
	 */
	protected function setCreated($created) {
		$this->setDataKey('created',$created);
	}

	/*
	 * get changed date
	 * 
	 * @return string created
	 */
	public function getChanged() {
		return $this->getDataKey('changed');
	}

	/*
	 * set changed date
	 * 
	 * @param string created
	 */
	protected function setChanged($changed) {
		$this->setDataKey('changed',$changed);
	}
	
	/*
	 * get registryWhenDate
	 * 
	 * @return string registryWhenDate
	 */
	public function getRegistryWhenDate() {
		return $this->getDataKey('registry_when_date');
	}
	
	/*
	 * set registryWhenDate 
	 * 
	 * @param string registryWhenDate
	 */
	protected function setRegistryWhenDate($registryWhenDate) {
		$this->setDataKey('registry_when_date',$registryWhenDate);
	}

	/*
	 * get GainingRegistrar of a cancelation
	 * this is required for PREACKS and some TLDS eg UK
	 * 
	 * @return string gainingRegistrar
	 */
	public function getGainingRegistrar() {
		return $this->getDataKey('gaining_registrar');
	}

	/*
	 * set GainingRegistrar of a cancelation
	 * this is required for PREACKS and some TLDS eg UK
	 * 
	 * @param string gainingRegistrar
	 */
	public function setGainingRegistrar($gainingRegistrar) {
		$this->setDataKey('gaining_registrar', $gainingRegistrar);
	}
	
	/*
	 * Create a cancelation
	 */
	public function doCreate() {
		$request = array(
			'code' => '0103101',
			'cancelation' =>  $this->getData()
		);
		$response=$this->send($request);
		$hash = $response->getDataAsArray();
		$this->fromArray($hash['cancelation']);
		return $response;
	}

	/*
	 * Update a cancelation
	 */
	public function doUpdate() {
		$request = array(
			'code' => '0103102',
			'cancelation' =>  $this->getData()
		);
		$response=$this->send($request);
		$hash = $response->getDataAsArray();
		$this->fromArray($hash['cancelation']);
		return $response;
	}

	/*
	 * Delete a cancelation
	 */	
	public function doDelete() {
		$request = array(
			'code' => '0103103',
			'cancelation' => array(
				'domain' => $this->getDomain(),
			),
		);
		return $this->send($request);
	}
	
	/*
	 * get Info from a Cancelation
	 */
	public function doInfo() {
		$request = array(
			'code' => '0103104',
			'cancelation' => array(
				'domain' => $this->getDomain(),
			),
		);
		$response=$this->send($request);
		$hash = $response->getDataAsArray();
		$this->fromArray($hash['cancelation']);
		return $response;
	}

	/*
	 * return a cancelation as array data
	 * 
	 * @return array cancelationdata
	 */
	public function toArray() {
		return $this->getData();
	}

	/*
	 * initialize a cancelation from array
	 * 
	 * @param array cancelationdata
	 */
	public function fromArray(Array $array=null) {
		if (!$array){
			return;
		}
		
		$this->setDomain($array['domain']);
		$this->setExecDate($array['execdate']);
		$this->setDisconnect($array['disconnect']);
		$this->setType($array['type']);	
		$this->setCreated($array['created']);
		$this->setChanged($array['changed']);
		$this->setRegistryWhenDate($array['registry_when_date']);	
	}

}
