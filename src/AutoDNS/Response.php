<?php

/*
Copyright (c) 2015 Marco Schrieck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/



namespace AutoDNS;

/**
 * A gerneric Response Class
 *
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class Response {

	private $response;
	private $responseXML;
	private $request;
	
	function getResponse() {
		return $this->response;
	}
	function getResponseXML() {
		return $this->responseXML;
	}
	function getRequest() {
		return $this->request;
	}

	function setResponse($response) {
		$this->response = $response;
	}

	function setRequest($request) {
		$this->request = $request;
	}

	public function parse($xml) {
		$this->responseXML = $xml;
		$hash = simplexml_load_string($xml);

		$this->response = $hash;
	}

	/*
	 * Temorarray Errors
	 *   - such as System Busy etc...
	 *   - connections Errors
	 *   - Authentication Errors
	 *   - HTTP Errors
	 * 
	 * @todo: implement.	
	 * 
	 * @return boolean
	 */

	public function isTempError() {
		// this is also one.
		// "EF01030" >Ein identischer Auftrag ist gerade in Bearbeitung.
		return false;
	}

	/*
	 * task is error
	 * 
	 * @return boolean
	 */

	public function isError() {
		return $this->response->result && $this->response->result->status && $this->response->result->status->type == 'error';
	}

	/*
	 * task is success
	 * 
	 * @return boolean
	 */

	public function isSuccess() {
		return $this->response->result && $this->response->result->status && $this->response->result->status->type == 'success';
	}

	/*
	 * task is not finished (status == 'notify' )
	 * 
	 * @return boolean
	 */

	public function isNotify() {
		return $this->response->result && $this->response->result->status && $this->response->result->status->type == 'notify';
	}

	public function getStatusText() {
		if ($this->response->result && $this->response->result->status) {
			return (string) $this->response->result->status->text;
		}
	}

	/*
	 * get Status + all Messages 
	 * @TODO not all messages are returned
	 */
	public function getMessages() {
		if(! $this->response->result){
			return "No Response";
		}
		$msgs=array();
		if ( $this->response->result->status) {
			$msgs[] = (string) $this->response->result->status->text;
		}
		if ( $msg=$this->response->result->msg  ) {
			if ( is_array($msg) ) {
				foreach ($msg as $m){
						$msgs[] = "; ". (string) $m->text . " (".$m->code.")";	
				}
			} else {
				$msgs[] = "; ". (string) $msg->text. " (".$msg->code.")";	
			}
		}
		return join("\n", $msgs);
	}

	public function getStatus() {
		if ($this->response->result && $this->response->result->status) {
			return (string) $this->response->result->status->type;
		}
	}

	public function getStatusCode() {
		if ($this->response->result && $this->response->result->status) {
			return (string) $this->response->result->status->code;
		}
	}

	public function getStatusObjectType() {
		if ($this->response->result && $this->response->result->status && $this->response->result->status->object) {
			return (string) $this->response->result->status->object->type;
		}
	}

	public function getStatusObjectValue() {
		if ($this->response->result && $this->response->result->status && $this->response->result->status->object) {
			return (string) $this->response->result->status->object->value;
		}
	}

	public function getDataAsArray() {
		if ($this->response->result && $this->response->result->data) {
			return json_decode(json_encode($this->response->result->data), 1);
		}
	}

	public function getData() {
		if ($this->response->result && $this->response->result->data) {
			return $this->response->result->data;
		}
	}

	public function getCTID() {
		if ($this->response->ctid) {
			return (string) $this->response->ctid;
		}
	}

	public function getSTID() {
		if ($this->response->stid) {
			return $this->response->stid;
		}
	}

	public function toArray(SimpleXMLElement $xml) {
		$array = (array) $xml;

		foreach (array_slice($array, 0) as $key => $value) {
			if ($value instanceof SimpleXMLElement) {
				$array[$key] = empty($value) ? NULL : toArray($value);
			}
		}
		return $array;
	}
	

}
