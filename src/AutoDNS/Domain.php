<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

require_once 'AbstractTask.php';

/**
 * Domain
 *
 * Deletes are done via Cancelations !
 * 
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class Domain extends AbstractTask {
	/*
	 * Renew Action for RESTORE_ARGP and RESTORE_NE  (only renew)
	 */

	const ACTION_RESTORE_RENEW = 'RESTORE_RENEW';
	/*
	 * Renew Action for Restore in ARGP (only renew)
	 */
	const ACTION_RENEW_ARGP = 'RESTORE_ARGP';
	/*
	 * Renew Action for Restore if not expired (only renew)
	 */
	const ACTION_RESTORE_NE = 'RESTORE_NE';
	/*
	 * Renew Action for Restore in RGP Phase (expensive)
	 */
	const ACTION_RESTORE_RGP = 'RESTORE_RGP';
	/*
	 * Registrar Status
	 */
	const REGISTRAR_STATUS_ACTIVE = 'ACTIVE';
	const REGISTRAR_STATUS_HOLD = 'HOLD';
	const REGISTRAR_STATUS_HOLD_LOCK = 'HOLD_LOCK';
	const REGISTRAR_STATUS_LOCK = 'LOCK';
	/*
	 * Registry Status
	 */
	const REGISTRY_STATUS_ACTIVE = 'ACTIVE';
	const REGISTRY_STATUS_HOLD = 'HOLD';
	const REGISTRY_STATUS_HOLD_LOCK = 'HOLD_LOCK';
	const REGISTRY_STATUS_LOCK = 'LOCK';
	const REGISTRY_STATUS_AUTO = 'AUTO';

	public function getAction() {
		return $this->getDataKey('action');
	}

	public function setAction($action) {
		$this->setDataKey('action', $action);
	}

	public function getUsePrivacy() {
		return $this->getDataKey('use_privacy');
	}

	/*
	 * this is for automatic DNSSEC behaivior, NSSET must support it.
	 */

	public function getUseAutoDNSSEC() {
		return $this->getDataKey('use_autodnssec');
	}

	public function setUsePrivacy($usePrivacy) {
		$this->setDataKey('use_privacy', $usePrivacy ? 1 : 0);
	}

	public function setUseAutoDNSSEC($useAutoDNSSEC) {
		$this->setDataKey('use_autodnssec', $useAutoDNSSEC ? 1 : 0);
	}

	public function getDomainsafe() {
		return $this->getDataKey('domainsafe');
	}

	public function getNsGroup() {
		return $this->getDataKey('ns_group');
	}

	public function getRegistrarStatus() {
		return $this->getDataKey('registrar_status');
	}

	public function getRegistryStatus() {
		return $this->getDataKey('registry_status');
	}

	public function setRegistrarStatus($registrarStatus) {
		$this->setDataKey('registrar_status', $registrarStatus);
	}

	public function setRegistryStatus($registryStatus) {
		$this->setDataKey('registry_status', $registryStatus);
	}

	public function getName() {
		return $this->getDataKey('name');
	}

	private function getContact($type) {
		$contact = $this->getDataKey('ownerc');
		if (is_array($contact)) {
			$obj = new DomainContact();
			$obj->fromArray($contact);
			return $obj;
		} else {
			return $contact;
		}
	}

	public function getOwnerc() {
		return $this->getContact('ownerc');
	}

	public function getAdminc() {
		return $this->getContact('adminc');
	}

	public function getTechc() {
		return $this->getContact('techc');
	}

	public function getZonec() {
		return $this->getContact('zonec');
	}

	public function getNserver() {
		return $this->getDataKey('nserver');
	}

	public function getAuthinfo() {
		return $this->getDataKey('authinfo');
	}

	public function getPeriod() {
		return $this->getDataKey('period');
	}

	/*
	 * This is the Expire Date at Registry site, you may use getPayable()
	 * @see getPayable
	 * 
	 */

	public function getDomainExpire() {
		return $this->getDataKey('domain_expire');
	}

	/*
	 * This is the Expire Date at Registrar site
	 * 
	 */

	public function getPayable() {
		return $this->getDataKey('payable');
	}

	/*
	 * This is the Expire Date at Registrar site
	 */

	public function setPayable($payable) {
		$this->setDataKey('payable', $payable);
	}

	public function getCreateDate() {
		return $this->getDataKey('created');
	}

	public function getConfirmOrder() {
		return $this->getDataKey('confirm_order') == '1';
	}

	/*
	 * @return boolean 
	 */

	public function getRemoveCancelation() {
		return $this->getDataKey('remove_cancellation') == 'yes';
	}

	public function setName($name) {
		$this->setDataKey('name', $name);
	}

	private function setContact($type, $contact) {
		if (is_a($contact, "AutoDNS\DomainContact")) {
			$contact = $contact->toArray();
		}
		$this->setDataKey($type, $contact);
	}

	/*
	 * @param ID or a AutoDNS\DomainContact
	 */

	public function setOwnerc($contact) {
		$this->setContact('ownerc', $contact);
	}

	public function setAdminc($contact) {
		$this->setContact('adminc', $contact);
	}

	public function setTechc($contact) {
		$this->setContact('techc', $contact);
	}

	public function setZonec($contact) {
		$this->setContact('zonec', $contact);
	}

	/*
	 * add a NServer to List of NServer
	 * if name is empty nothing is set
	 * 
	 * @usage  addNServer('ns1.test.de','1.2.3.4','ff::1','2.2.2.2') 
	 *  
	 * @param name - nameserver name
	 * @param ips  - set op ips mixed IPv4 or IPv6  needed for glue records
	 * 
	 */

	public function addNServer($name) {
		$args = func_get_args();
		$name = array_shift($args);

		if (!$name) {
			return;
		}

		$ipv4 = array();
		$ipv6 = array();
		foreach ($args as $ip) {
			if (preg_match("^\d+\.\d+\.\d+\.\d+$", $ip)) {
				array_push($ipv4, $ip);
			} else {
				array_push($ipv6, $ip);
			}
		}

		$nserver = array(
			'name' => $name,
		);
		if (count($ipv4)) {
			$nserver['ip'] = $ipv4;
		}

		if (count($ipv6)) {
			$nserver['ip6'] = $ipv;
		}
		$this->pushDataKey('nserver', $nserver);
	}

	public function setAuthinfo($authinfo) {
		$this->setDataKey('authinfo', $authinfo);
	}

	public function setPeriod($period) {
		$this->setDataKey('period', (int) $period);
	}

	public function setConfirmOrder($confirmOrder) {
		$this->setDataKey('confirm_order', $confirmOrder ? 1 : 0);
	}

	/*
	 * Remove cancelation 
	 */

	public function setRemoveCancelation($removeCancelation) {
		if ($removeCancelation) {
			$this->setDataKey('remove_cancellation', 'yes');
		} else {
			$this->unsetDataKey('remove_cancellation');
		}
	}

	// tasks

	public function doCreate() {
		$request = $this->transform("0101");
		return $this->send($request);
	}

	public function doUpdate() {
		$request = $this->transform("0102");
		return $this->send($request);
	}

	public function doTransfer() {
		$request = $this->transform("0104");
		return $this->send($request);
	}

	public function toArray() {
		return $this->getData();
	}

	public function fromArray(Array $array = null) {
		if (!$array) {
			return;
		}
		$this->setName(isset($array['name']) ? $array['name'] : null);
		$this->setOwnerc(isset($array['ownerc']) ? $array['ownerc'] : null);
		$this->setAdminc(isset($array['adminc']) ? $array['adminc'] : null);
		$this->setTechc(isset($array['techc']) ? $array['techc'] : null);
		$this->setZonec(isset($array['zonec']) ? $array['zonec'] : null);
		$this->setPeriod(isset($array['period']) ? $array['period'] : null);
		$this->setAuthinfo(isset($array['authinfo']) ? $array['authinfo'] : null);
		$this->setConfirmOrder(isset($array['confirm_order']) ? $array['confirm_order'] : null);
		$this->setPayable(isset($array['payable']) ? $array['payable'] : null);
		$this->setRegistrarStatus($array['registrar_status']);
		$this->setRegistryStatus(isset($array['registry_status']) ? $array['registry_status'] : null);
		$this->setAction(isset($array['action']) ? $array['action'] : null);
		$this->setRemoveCancelation(isset($array['remove_cancelation']) ? $array['remove_cancelation'] : null );
		$this->setUseAutoDNSSEC(isset($array['use_autodnssec']) ? $array['use_autodnssec'] : null);
		$this->setUsePrivacy($array['use_privacy']);

		/*
		 * @todo ips check
		 */
		$nserver = isset($array['nserver']) ? $array['nserver'] : null;
		if ($nserver && is_array($nserver)) {
			foreach ($nserver as $ns) {
				if (is_array($ns)) {
					$this->addNServer(isset($ns['name']) ? $ns['name'] : null);
				}
			}
		}
	}

	private function transform($code) {
		$data = $this->data;
		return $hash = array(
			'code' => $code,
			'domain' => $data
		);
	}

	/*
	 * Set Registy Status at Registry
	 */

	public function doUpdateStatus() {
		$request = array(
			'code' => '0102002',
			'domain' => array(
				'name' => $this->getName(),
				'registry_status' => $this->getRegistryStatus(),
			),
		);
		return $this->send($request);
	}

	/*
	 * Inquire Information for a Domain
	 * 
	 * Hint:
	 * 	- for full Contact Data use option=array('fullcontact'=>true)
	 * 
	 * @param array options
	 * 
	 */

	public function doInfo($options = array()) {
		$request = array(
			'code' => '0105',
			'domain' => array(
				'name' => $this->getName(),
			),
			'key' => 'payable',
		);

		// Inquire Full Contact Data
		if (is_array($options) && array_key_exists('fullcontact', $options) && $options['fullcontact']) {
			$request['show_handle_details'] = 'ownerc,adminc,techc,zonec';
		}

		$response = $this->send($request);
		$hash = $response->getDataAsArray();
		$this->fromArray($hash['domain']);
		return $response;
	}

	/*
	 * Renew a Domain
	 * 
	 * @param name
	 * @param payable
	 * @param period
	 * @param remove_cancellation
	 */

	public function doRenew() {
		$request = array(
			'code' => '0101003',
			'domain' => array(
				'name' => $this->getName(),
				'payable' => $this->getPayable(),
				'period' => $this->getPeriod(),
				'remove_cancellation' => $this->getRemoveCancelation()
			),
		);
		return $this->send($request);
	}

	public function doCreateAuthinfo1(){
		$request = array(
			'code' => '0113001',
			'domain' => array(
				'name' => $this->getName()
			),
		);
		return $this->send($request);
	}

	/*
	 * Restore a domain
	 * 
	 * @param name
	 * @param action
	 * 
	 */

	public function doRestore() {
		$request = array(
			'code' => '0101005',
			'domain' => array(
				'name' => $this->getName(),
				'action' => $this->getAction()
			),
		);
		return $this->send($request);
	}

}
