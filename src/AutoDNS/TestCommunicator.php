<?php

/*
Copyright (c) 2015 Marco Schrieck

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is furnished
to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/



namespace AutoDNS;

require_once 'ICommunicator.php';

/**
 * Simple MOCK Communicator to test AutoDNS Task
 *
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
class TestCommunicator implements ICommunicator {

	private $request;
	private $response;

	/*
	 * get the request which is transmited via send
	 * 
	 * @param
	 * @return request (text) 
	 */

	public function getRequest() {
		return $this->request;
	}

	/*
	 * set a response which is used by send
	 * 
	 * @param response (text)
	 * @return 
	 */

	public function setResponse($response) {
		$this->response = $response;
	}

	/*
	 * Return a given response and store a request for later usage.
	 * This function is called from Task Classes
	 * 
	 * @param  request
	 * @return response
	 */

	public function send($request) {
		$this->request = $request;
		return $this->response;
	}

}
