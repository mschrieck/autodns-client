<?php

/*
  Copyright (c) 2015 Marco Schrieck

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is furnished
  to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
 */

namespace AutoDNS;

require_once 'AbstractTask.php';

/**
 * Domain
 *
 * Deletes are done via Cancelations !
 * 
 * @author Marco Schrieck <mschrieck@gmail.com>
 */
abstract class AbstractListInquire extends AbstractTask {

	protected $response;
	protected $objects = array();
	protected $summary = 0;

	const WHERE_OP_EQUAL = 'eq';
	const WHERE_OP_NO_EQUAL = 'ne';
	const WHERE_OP_LIKE = 'like';
	const WHERE_OP_GREATER_THEN = 'gt';
	const WHERE_OP_LOWER_THEN = 'lt';
	const WHERE_OP_GREATER_OR_EQUAL = 'ge';
	const WHERE_OP_LOWER_OR_EQUAL = 'le';
	const ORDER_MODE_DESC = 'desc';
	const ORDER_MODE_ASC = 'asc';

	/*
	 * This function must be implemented to get the TaskCode
	 * 
	 * @return taskcode
	 */

	abstract public function getTaskCode();

	/*
	 * This function must be implemented to parse a hash and return it as obj
	 * 
	 * @return array $obj
	 */

	abstract public function parseListObject($data);

	/*
	 * this function must be implemented to get the Object key in reponse
	 */

	abstract public function getObjectName();

	/*
	 * set a value in the view part
	 */

	private function setViewValue($key, $value) {

		$view = $this->getDataKey('view');
		if (!is_array($view)) {
			$view = array();
		}
		$view[$key] = $value;

		$this->setDataKey('view', $view);
	}

	public function __construct(Auth $auth) {
		parent::__construct($auth);
		$this->setDataKey('view', array());
		$this->setDataKey('where', array());
	}

	public function setChildren($children) {
		$this->setViewValue('children', $children ? 1 : 0);
	}

	public function getChildren() {
		$view = $this->getDataKey('view');
		return $view['children'] ? true : false;
	}

	public function setLimit($limit) {
		$this->setViewValue('limit', $limit);
	}

	public function getLimit() {
		$view = $this->getDataKey('view');
		return $view['limit'];
	}

	public function setOffset($offset) {
		$this->setViewValue('offset', $offset);
	}

	public function getOffset() {
		$view = $this->getDataKey('view');
		return $view['offset'];
	}

	/*
	 * For some history task a date range is needed
	 */

	public function setTo($to) {
		$this->setViewValue('to', $to);
	}

	/*
	 * For some history task a date range is needed
	 */

	public function getTo() {
		$view = $this->getDataKey('view');
		return $view['to'];
	}

	/*
	 * For some history task a date range is needed
	 */

	public function setFrom($from) {
		$this->setViewValue('from', $from);
	}

	/*
	 * For some history task a date range is needed
	 */

	public function getFrom() {
		$view = $this->getDataKey('view');
		return $view['from'];
	}

	/*
	 * Add a key for the result set
	 */

	public function addKey($key) {
		$keys = $this->getDataKey('key');

		if (!is_array($keys)) {
			$keys = array();
		}
		array_push($keys, (string) $key);
		$this->setDataKey('key', $keys);
	}

	public function getKey() {
		return $this->getDataKey('key');
	}

	/*
	 * Add a key for the result set
	 */

	public function addOrder($key, $mode) {
		$order = $this->getDataKey('order');
		if (!is_array($order)) {
			$order = array();
		}

		array_push($order, array(
			'key' => (string) $key,
			'mode' => (string) $mode
		));

		$this->setDataKey('order', $order);
	}

	public function getOrder() {
		return $this->getDataKey('order');
	}

	/*
	 * Generate a Single Where Construkt
	 */

	public function getWhere() {
		return $this->getDataKey('where');
	}

	public function setWhere($where) {
		$this->setDataKey('where', $where);
	}

	/*
	 * Generate a Single Where Construkt
	 */

	static public function buildWhere($key, $op, $value) {
		return array(
			'key' => (string) $key,
			'operator' => (string) $op,
			'value' => (string) $value
		);
	}

	public function toArray() {
		
	}

	public function fromArray(Array $array) {
		$data = array();
	}

	/*
	 * get List
	 */

	public function doList() {

		$request = array(
			'code' => $this->getTaskCode(),
		);

		if ($view = $this->getDataKey('view')) {
			$request['view'] = $view;
		}

		if ($key = $this->getDataKey('key')) {
			$request['key'] = $key;
		}

		if ($order = $this->getDataKey('order')) {
			$request['order'] = $order;
		}

		if ($where = $this->getDataKey('where')) {
			$request['where'] = $where;
		}

		$this->response = $this->send($request);


		// parse
		if ($this->response && $data = $this->response->getDataAsArray()) {
			if (is_array($data)) {
				$this->parseListObjects($data);

				$this->summary = $data['summary'];
			}
		}
		return $this->response;
	}

	/*
	 * @return list of Item Objects from a List
	 */

	protected function parseListObjects($data) {
		if(!$data && is_array($data)){
			return;
		}
		
		$objects = isset( $data[$this->getObjectName()] ) 
					? $data[$this->getObjectName()] 
					: null;

		// dont parse empty lists
		if (!$objects) {
			return;
		}
		// force array
		if (!is_array($objects)) {
			$objects = array($objects);
		}

		foreach ($objects as $object) {
			array_push($this->objects, $this->parseListObject($object));
		}
	}

	public function getObjects() {
		return $this->objects;
	}

	public function getSummary() {
		return $this->summary;
	}

}
