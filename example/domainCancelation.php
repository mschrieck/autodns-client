<?php

/*
 * Cancelation process
 * first a domainCreate is called
 */

require_once 'domainCreate.php';

$domainCancelation = new AutoDNS\DomainCancelation($auth);
$domainCancelation->setDomain($domainName);
$domainCancelation->setType(\AutoDNS\DomainCancelation::TYPE_DELETE);
$domainCancelation->setExecDate( date('Y-m-d', strtotime('+ 2 month')) );

$response=$domainCancelation->doCreate();
logAPICall("DomainCancelationCreate",$response);

$response=$domainCancelation->doUpdate();
logAPICall("DomainCancelationUpdate",$response);


$response=$domainCancelation->doInfo();
logAPICall("DomainCancelationInfo",$response);
print_r($domainCancelation->toArray());

$response=$domainCancelation->doDelete();
logAPICall("DomainCancelationDelete",$response);

$domainCancelation = new AutoDNS\DomainCancelation($auth);
$domainCancelation->setDomain($domainName);
$domainCancelation->setType(\AutoDNS\DomainCancelation::TYPE_DELETE);
$domainCancelation->setExecDate( \AutoDNS\DomainCancelation::EXECDATE_NOW );
$response=$domainCancelation->doCreate();

logAPICall("DomainCancelationCreate",$response);
$response=$domainCancelation->doInfo();
logAPICall("DomainCancelationInfo",$response);
print_r($domainCancelation->toArray());




?>