<?php

/*
 * Basic Initialization and Helper Functions
 */

require_once 'AutoDNS/Auth.php';
require_once 'AutoDNS/Domain.php';
require_once 'AutoDNS/DomainContact.php';
require_once 'AutoDNS/DomainCancelation.php';
require_once 'AutoDNS/DefaultCommunicator.php';
require_once 'AutoDNS/AbstractListInquire.php';
require_once 'AutoDNS/DomainJobList.php';
require_once 'AutoDNS/DomainJobHistoryList.php';

date_default_timezone_set('Europe/Berlin');

$domainName = randomstring()."-test-" . uniqid() . "-example.com";

$com = new AutoDNS\DefaultCommunicator();
$com->setUrl($config['url']);
$com->setUserAgent("AutoDNS Client Example");

// Config Tasks 
$auth = new AutoDNS\Auth($com);
$auth->setUser($config['user']);
$auth->setContext($config['context']);
$auth->setPassword($config['password']);


/*
 * ---------
 *  Helper
 * ---------
 */

function randomstring() {
	return substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
};


function logAPICall($text, AutoDNS\Response $response) {
	if (!$response) {
		error_log($text, 'No Response for logging');
		return;
	}
	$log = "AutoDNS: Call $text\n";
	$log.="   REQUEST:\n\n" . $response->getRequest() . "\n\n";
	$log.="   RESPONSE:\n\n" . $response->getResponseXML() . "\n\n";
	error_log($log);
}
