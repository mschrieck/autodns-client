<?php

require_once 'config.php';
require_once 'basic.php';


$ctid = uniqid();

$ownerc = new AutoDNS\DomainContact($auth);
$ownerc->setFName( randomstring(). "fname");
$ownerc->setLName( randomstring(). "lname ");
$ownerc->setCity( randomstring()."city");
$ownerc->setOrganization( randomstring()."org " );
$ownerc->setAddress( randomstring()."address ");
$ownerc->setPCode("pcode");
$ownerc->setCountry("DE");
$ownerc->setPhone("+49-123-123");
$ownerc->setEmail("info@example.com");
$ownerc->setType(AutoDNS\DomainContact::TYPE_ORG);


$domain = new AutoDNS\Domain($auth);
$domain->setName($domainName);
$domain->setOwnerc($ownerc);
$domain->setAdminc($ownerc);
$domain->setTechc($ownerc);
$domain->setZonec($ownerc);
$domain->addNServer("ns1.sedoparking.com");
$domain->addNServer("ns2.sedoparking.com");
$domain->setCTID($ctid);

$response = $domain->doCreate();


print "===========\n";
print $response->getStatusText() . "\n";
print $response->getStatusObjectType() . ': ' . $response->getStatusObjectValue() . "\n";
print "===========\n";


$where = AutoDNS\AbstractListInquire::buildWhere('object', \AutoDNS\AbstractListInquire::WHERE_OP_EQUAL, $domainName);

$i = 0;
while ($i < 20) {
	$jobInquire = new AutoDNS\DomainJobList($auth);
	$jobInquire->setChildren(0);
	$jobInquire->setWhere($where);
	$response = $jobInquire->doList();

	$i++;
	if ($response->isSuccess() && $jobInquire->getSummary() != 0) {
		$object = $jobInquire->getObjects()[0];
		print_r($object);


		print "===========\n";
		print "STATUS ($i): " . $object->getStatus() . " - " . $object->getSubStatus() . "\n";
		print "===========\n";
		sleep(10);
	} else {
		continue;
	}
}

$jobHistoryInquire = new AutoDNS\DomainJobHistoryList($auth);
$jobHistoryInquire->setChildren(0);
$jobHistoryInquire->setLimit(1);
$jobHistoryInquire->setTo(date("Y-m-d 23:59:59"));
$jobHistoryInquire->setFrom(date("Y-m-d 00:00:00", strtotime("-1 week")));
$jobHistoryInquire->setWhere($where);
$jobHistoryInquire->addOrder('id', "desc");

$response = $jobHistoryInquire->doList();
print_r($jobHistoryInquire->getObjects());

$status = '';
$historyObjs = $jobHistoryInquire->getObjects();
if ( is_array($historyObjs) ) {
	$historyObj = $historyObjs[0];
}




print "===========\n";
print "END STATUS: " . $historyObj->getStatus() . "\n";
print "===========\n";


